//go:generate gorunpkg github.com/99designs/gqlgen

package server

import (
	context "context"
	"errors"
	"net/http"
	"strings"

	"github.com/99designs/gqlgen/handler"
	"gitlab.com/the.nasso/daily-digest/daily_digest"
)

type App struct {
	accessDenied error
}

type Mutations struct {
}

type Queries struct {
}

type contextKey string

func NewApp() *App {
	return &App{}
}

func (app *App) Seed() {
	daily_digest.SeedEntries()
}

func (app *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Setup response headers
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	// Add auth to context
	c := r.Context()
	sessionKey := r.Header.Get("Authorization")
	sessionKey = strings.Replace(sessionKey, "Bearer ", "", 1)
	user := daily_digest.GetUserForSession(sessionKey)
	c = context.WithValue(c, contextKey("user"), user)
	r = r.WithContext(c)

	// Handle request
	handler.GraphQL(daily_digest.NewExecutableSchema(daily_digest.Config{Resolvers: app}))(w, r)
}

/*
	NewSubscription(ctx context.Context, input *NewSubscriptionInput) (Subscription, error)
	Register(ctx context.Context, input RegisterInput) (*string, error)
	Login(ctx context.Context, input LoginInput) (*string, error)
*/

func (app *App) Mutation() daily_digest.MutationResolver {
	return Mutations{}
}

func (app *App) Query() daily_digest.QueryResolver {
	return Queries{}
}

func (m Mutations) NewSubscription(ctx context.Context, input *daily_digest.NewSubscriptionInput) (daily_digest.Subscription, error) {
	user := currentUser(ctx)
	if user == nil {
		return daily_digest.Subscription{}, errors.New("Access denied")
	}

	source := daily_digest.GetSourceById(*input.SourceID)
	if source == nil {
		return daily_digest.Subscription{}, errors.New("Invalid sourceId")
	}
	return *daily_digest.CreateSubscription(user, source), nil
}

func (m Mutations) Register(ctx context.Context, input daily_digest.RegisterInput) (*string, error) {
	daily_digest.RegisterUser(input.Username, input.Password)
	return daily_digest.Login(input.Username, input.Password)
}

func (m Mutations) Login(ctx context.Context, input daily_digest.LoginInput) (*string, error) {
	return daily_digest.Login(input.Username, input.Password)
}

func (q Queries) Sources(ctx context.Context) ([]daily_digest.Source, error) {
	sources := make([]daily_digest.Source, 0)
	for _, source := range daily_digest.GetSources() {
		sources = append(sources, *source)
	}

	return sources, nil
}

func (q Queries) Subscriptions(ctx context.Context) ([]daily_digest.Subscription, error) {
	user := currentUser(ctx)
	if user == nil {
		return []daily_digest.Subscription{}, errors.New("Access denied")
	}
	return daily_digest.ListAllSubscriptions(user), nil
}

func (q Queries) Digests(ctx context.Context, date string) ([]daily_digest.Digest, error) {
	user := currentUser(ctx)
	if user == nil {
		return []daily_digest.Digest{}, errors.New("Access denied")
	}
	return daily_digest.GetDigest(user, date), nil
}

func currentUser(c context.Context) *daily_digest.User {
	user, ok := c.Value(contextKey("user")).(*daily_digest.User)
	if ok {
		return user
	}
	return nil
}
