default:
	go run main.go

deps:
	curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
	dep ensure
	go build -o gqlgen vendor/github.com/99designs/gqlgen/main.go

generate:
	./gqlgen -v

build:
	go build

test:
	go test plugins/*_test.go
