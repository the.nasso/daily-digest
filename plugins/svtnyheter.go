package plugins

import (
	"time"

	"gitlab.com/the.nasso/daily-digest/daily_digest"
)

func init() {
	duration, _ := time.ParseDuration("5s")
	daily_digest.RegisterSource(
		"svtnyheter",
		"SvtNyheter",
		"News in swedish",
		[]string{"news", "swedish"},
		noopScraper,
		duration,
		noopPersister,
	)
}
