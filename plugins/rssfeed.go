package plugins

import (
	"time"

	"gitlab.com/the.nasso/daily-digest/daily_digest"
)

func init() {
	duration, _ := time.ParseDuration("5s")
	daily_digest.RegisterSource(
		"rss",
		"RSS Feed",
		"Any RSS-feed. Great for blogs, podcasts and more.",
		[]string{"blog", "podcast", "general"},
		noopScraper,
		duration,
		noopPersister,
	)
}
