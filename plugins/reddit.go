package plugins

import (
	"time"

	"gitlab.com/the.nasso/daily-digest/daily_digest"
)

func init() {
	duration, _ := time.ParseDuration("5s")
	daily_digest.RegisterSource(
		"reddit",
		"Reddit",
		"The top posts of any subreddit.",
		[]string{"news", "aggregator"},
		noopScraper,
		duration,
		noopPersister,
	)
}
