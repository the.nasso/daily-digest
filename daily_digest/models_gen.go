// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package daily_digest

type LoginInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type NewSubscriptionInput struct {
	SourceID *string `json:"sourceId"`
}

type RegisterInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
