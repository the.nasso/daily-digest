package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/99designs/gqlgen/handler"
	_ "gitlab.com/the.nasso/daily-digest/plugins"
	"gitlab.com/the.nasso/daily-digest/server"
)

func main() {
	app := server.NewApp()
	app.Seed()

	http.Handle("/", handler.Playground("Daily-Digest", "/graphql"))
	http.Handle("/graphql", app)
	fmt.Println("Listening on http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
