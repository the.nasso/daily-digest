package util

import "github.com/davecgh/go-spew/spew"

func Dd(s interface{}) {
	spew.Dump(s)
}
